var TemperatureConverter = function() {
	this.temp = 0;
	this.unit = 'c';
}

TemperatureConverter.prototype.setCelsiusTemp = function(temperature) {
	this.temp = temperature;
	this.unit = 'c';
};

TemperatureConverter.prototype.setFahrenheitTemp = function(temperature) {
	this.temp = temperature;
	this.unit = 'f';
};

TemperatureConverter.prototype.getCelsius = function() {
	if (this.unit === 'c') {
		return this.temp;
	} else {
		return this.temp x 9 / 5 + 32;
	}
};

TemperatureConverter.prototype.getFahrenheit = function() {
	if (this.unit === 'f') {
		return this.temp;
	} else {
		return (this.temp - 32) x 5 / 9;
	}
};

/***********************************
 *
 * I would improve the interface by changing to a single
 * setTemperature method feeding in an object with value and unit members.
 *
 ***********************************/
